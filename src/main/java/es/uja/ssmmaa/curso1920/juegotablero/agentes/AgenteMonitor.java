/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.juegotablero.agentes;

import es.uja.ssmmaa.curso1920.juegotablero.gui.ConsolaJFrame;
import es.uja.ssmmaa.curso1920.juegotablero.tareas.SubscripcionDF;
import es.uja.ssmmaa.curso1920.juegotablero.tareas.TareaSubscripcionDF;
import es.uja.ssmmaa.curso1920.ontologia.Vocabulario;
import static es.uja.ssmmaa.curso1920.ontologia.Vocabulario.JUEGOS;
import static es.uja.ssmmaa.curso1920.ontologia.Vocabulario.TIPOS_SERVICIO;
import es.uja.ssmmaa.curso1920.ontologia.Vocabulario.TipoJuego;
import es.uja.ssmmaa.curso1920.ontologia.Vocabulario.TipoServicio;
import static es.uja.ssmmaa.curso1920.ontologia.Vocabulario.TipoServicio.JUGADOR;
import static es.uja.ssmmaa.curso1920.ontologia.Vocabulario.TipoServicio.ORGANIZADOR;
import static es.uja.ssmmaa.curso1920.ontologia.Vocabulario.TipoServicio.TABLERO;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author pedroj
 * Esqueleto de agente para la estructura general que deben tener todos los
 * agentes
 */
public class AgenteMonitor extends Agent implements SubscripcionDF {
    //Variables del agente
    private ConsolaJFrame gui;
    private Map<String,List<AID>> agentesConocidos;
    
    @Override
    protected void setup() {
       //Inicialización de las variables del agente
       gui = new ConsolaJFrame("AGENTE", this.getLocalName());
       gui.setVisible(true);
       
       agentesConocidos = new HashMap();
       for(TipoServicio servicio : TIPOS_SERVICIO)
           for(TipoJuego juego : JUEGOS)
               agentesConocidos.put(servicio.name()+juego.name(), new ArrayList());
       
       //Configuración del GUI
       
       //Registro del agente en las Páginas Amarrillas
       
       //Registro de la Ontología
       
       System.out.println("Se inicia la ejecución del agente: " + this.getName());
       //Añadir las tareas principales
       DFAgentDescription template = new DFAgentDescription();
       ServiceDescription templateSd = new ServiceDescription();
       templateSd.setType(JUGADOR.name());
       template.addServices(templateSd);
       addBehaviour(new TareaSubscripcionDF(this,template));
       templateSd.setType(TABLERO.name());
       template.addServices(templateSd);
       addBehaviour(new TareaSubscripcionDF(this,template));
       templateSd.setType(ORGANIZADOR.name());
       template.addServices(templateSd);
       addBehaviour(new TareaSubscripcionDF(this,template));
    }

    @Override
    protected void takeDown() {
       //Eliminar registro del agente en las Páginas Amarillas
       
       //Liberación de recursos, incluido el GUI
       
       //Despedida
       System.out.println("Finaliza la ejecución del agente: " + this.getName());
    }
    
    //Métodos de trabajo del agente
    
    
    //Clases internas que representan las tareas del agente

    @Override
    public void addAgent(AID agente, Vocabulario.TipoJuego juego, String tipoServicio) {
        List jugadores = agentesConocidos.get(tipoServicio+juego.name());
        jugadores.add(agente);
        String salida = "SE HA REGISTRADO\n" +
                        "Agente : " + agente.getLocalName() + " Juego : " + juego +
                        " Servicio : " + tipoServicio;
        gui.presentarSalida(salida);
    }

    @Override
    public boolean removeAgent(AID agente, Vocabulario.TipoJuego juego, String tipoServicio) {
        boolean resultado = false;
        
        List jugadores = agentesConocidos.get(tipoServicio+juego.name());
        resultado = jugadores.remove(agente);
        
        if( resultado ) {
            String salida = "SE ELIMINADO DEL REGISTRO\n" +
                        "Agente : " + agente.getLocalName() + " Juego : " + juego +
                        " Servicio : " + tipoServicio;
            gui.presentarSalida(salida);
        }
        
        return resultado;
    }   
}
